package com.hanxiaozhang.http.dispatcher;


import com.hanxiaozhang.http.config.Context;

/**
 * 功能描述: <br>
 * 〈分发器/调度器〉
 *
 * @Author:hanxiaozhang
 * @Date: 2022/1/7
 */
public interface Dispatcher {

    Object dispatch(Context Context);

}
