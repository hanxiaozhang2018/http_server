package com.hanxiaozhang.http.dispatcher;

import io.netty.handler.codec.http.HttpResponseStatus;

import java.util.HashMap;
import java.util.Map;

/**
 * 功能描述: <br>
 * 〈〉
 *
 * @Author:hanxiaozhang
 * @Date: 2022/1/30
 */
public class ResponseModel {

    private HttpResponseStatus status;

    private Object content;

    private Map<String, Object> headers = new HashMap<>();

    public HttpResponseStatus getStatus() {
        return status;
    }

    public void setStatus(HttpResponseStatus status) {
        this.status = status;
    }

    public Object getContent() {
        return content;
    }

    public void setContent(Object content) {
        this.content = content;
    }

    public Map<String, Object> getHeaders() {
        return headers;
    }

    public void setHeader(String headerName, Object headerValue) {
        headers.put(headerName, headerValue);
    }
}
