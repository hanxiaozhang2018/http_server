package com.hanxiaozhang.http.dispatcher;

import com.hanxiaozhang.http.BaseController;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 功能描述: <br>
 * 〈缓存所有Mapper〉
 *
 * @Author:hanxiaozhang
 * @Date: 2022/1/7
 */
public class HandlerMapper {

    private Map<String, BaseController> mapper = new ConcurrentHashMap<>();

    /**
     * 注册Mapper
     *
     * @param key
     * @param processor
     */
    public void registerHandler(String key, BaseController processor){
        this.mapper.put(key,processor);
    }

    /**
     * 获取Mapper
     *
     * @param key
     * @return
     */
    public BaseController get(String key){
        return mapper.get(key);
    }


}
