package com.hanxiaozhang.http.annotions;

import java.lang.annotation.*;

/**
 * 功能描述: <br>
 * 〈Controller类标识〉
 *
 * @Author:hanxiaozhang
 * @Date: 2022/1/30
 */
@Target({ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface WebController {

    String value() default "";

}
