package com.hanxiaozhang.http.annotions;

/**
 * 功能描述: <br>
 * 〈请求方法类型〉
 *
 * @Author:hanxiaozhang
 * @Date: 2022/1/30
 */
public enum RequestMethod {

    GET,
    HEAD,
    POST,
    PUT,
    PATCH,
    DELETE,
    OPTIONS,
    TRACE;

    RequestMethod() {
    }

}
