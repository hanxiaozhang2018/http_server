package com.hanxiaozhang.http.annotions;

import java.lang.annotation.*;

/**
 * 功能描述: <br>
 * 〈请求映射注解〉
 *
 * @Author:hanxiaozhang
 * @Date: 2022/1/30
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RequestMapping {

    String value();

    RequestMethod[] method() default {};

}
