package com.hanxiaozhang.http.config;

/**
 * 功能描述: <br>
 * 〈上下文加载〉
 *
 * @Author:hanxiaozhang
 * @Date: 2022/1/7
 */
public interface ContextLoader {

    ApplicationContext load(String classPath);

    ApplicationContext load(String classPath, String regx);

}
