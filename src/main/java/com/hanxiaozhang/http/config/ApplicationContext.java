package com.hanxiaozhang.http.config;

import com.hanxiaozhang.http.dispatcher.HandlerMapper;

/**
 * 功能描述: <br>
 * 〈应用上下文〉
 *
 * @Author:hanxiaozhang
 * @Date: 2022/1/7
 */
public class ApplicationContext {

    private HandlerMapper mapper;

    public ApplicationContext(){
        this.mapper = new HandlerMapper();
    }

    public HandlerMapper getMapper() {
        return mapper;
    }

    public void setMapper(HandlerMapper mapper) {
        this.mapper = mapper;
    }

}
