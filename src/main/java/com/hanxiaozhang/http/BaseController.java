package com.hanxiaozhang.http;

import com.hanxiaozhang.http.config.Context;

/**
 * 功能描述: <br>
 * 〈基础Controller〉
 *
 * @Author:hanxiaozhang
 * @Date: 2022/1/7
 */
public interface BaseController {

    Object process(Context context);

}
