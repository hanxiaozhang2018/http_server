package com.hanxiaozhang.controller;


import com.hanxiaozhang.http.BaseController;
import com.hanxiaozhang.http.annotions.RequestMapping;
import com.hanxiaozhang.http.annotions.RequestMethod;
import com.hanxiaozhang.http.annotions.WebController;
import com.hanxiaozhang.http.config.Context;

/**
 * 功能描述: <br>
 * 〈〉
 *
 * @Author:hanxiaozhang
 * @Date: 2022/1/7
 */
@WebController
public class TestController implements BaseController {

    @Override
    @RequestMapping(value = "test", method = {RequestMethod.GET, RequestMethod.POST})
    public Object process(Context context) {
        return "success";
    }

}
